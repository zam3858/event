<table>
	<tr>
		<td>Name</td> <td>{{ $user->name }}</td>
	</tr>
	<tr>
		<td>Email</td> <td>{{ $user->email }}</td>
	</tr>
	<tr>
		<td>State</td> <td>{{ $user->state->name ?? '' }}</td>
	</tr>
</table>