@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <select name="state_id" 
                        id="state_id" >
                        @foreach($states as $state)
                            <option value="{{ $state->id }}">
                                {{ $state->name }}
                            </option>
                        @endforeach
                    </select>

                    <select name="user_id" 
                        id="user_id" >

                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#state_id').on('change', function() {

            $.ajax({
                url: '{{ url('states') }}/' + $(this).val() + '/users',
                type: 'get',
                dataType : 'html',
                success: function(data){
                    $('#user_id').html(data);
                }
            });

        });
    });

</script>
@endsection