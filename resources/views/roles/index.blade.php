@extends('layouts.app')


@section('content')
    <div class="card">
		<div class="card-header header-elements-inline bg-green">
			<h5 class="card-title font-weight-semibold">{{ __('Senarai Role') }}</h5>
			<div class="header-elements">
				<div class="list-icons">
					<a class="list-icons-item" data-action="collapse"></a>
				</div>
			</div>
		</div>

		<div class="card-header bg-transparent header-element-inline">
			<div class="header-elements">
				<div class="text-right">
					<a href="{{ route('roles.create') }}" class="list-icon-items btn btn-primary btn-sm"><i class="icon-add"></i> {{__('Tambah Role')}}</a>
				</div>
			</div>
		</div>

        <div class="table-responsive">
            <table class="table table-hover">
				<thead>
					<tr class="bg-grey-300">
						<th>#</th>
						<th>
							<span class="font-weight-semibold">{{ __('Role') }} <a href="{{ route('roles.create') }}"><i class="icon-plus-circle2"></i></a></span>
							<i class="icon-plus ml-2"></i>
						</th>
						<th>Tindakan</th>
					</tr>
				</thead>
                <tbody>
				@php $bil = 1;@endphp
                @foreach($roles as $role)
                    <tr>
                        <td>{{ $bil++ }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <a href="{{ route('roles.edit' , $role->id )}}" class='btn btn-primary'><i class="icon-pencil"></i></a>
                            <a href="#" class='btn btn-danger' onclick="mform.delete({{ $role->id }}, 'del_{{ $role->id }}', '{{ __('Anda pasti?') }}' )"><i class="fa fa-trash-alt"></i></a>
                            <form action="{{ route('roles.destroy' , $role->id )}}" method="POST" id="del_{{ $role->id }}">
                                @csrf
                                @method('delete')
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    function delete_rec(id) {

    }
@endsection
