@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-body">
		<a href="{{ url('/roles') }}" class="btn btn-outline-primary btn-xs"><i class="fa fa-angle-double-left fa-sm" ></i>{{ __('Kembali') }}</a><p></p>
		<ul class="nav nav-tabs nav-tabs-highlight">
			<li class="nav-item"><a href="#left-icon-tab1" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i>{{ __('Tambah Role') }}</a></li>
		</ul>

	<div class="tab-content">
	<div class="tab-pane fade show active" id="left-icon-tab1">

    <div class="card-body">
        <form action="{{ route('roles.store') }}" method='POST'>
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-lg-2">{{ __('Nama Role') }}</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="name" value="{{ old('name')}}" required>
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
				Pilih Modul dibenarkan
                <table class="table table-hover">
					<thead>
                    <tr class="bg-grey-400">
                        <th>{{ __('Permission') }}</th>
                        <th>{{ __('Description') }}</th>
                        <th>{{ __('Tindakan') }}</th>
                    </tr>
					</thead>
                    @foreach($permissions as $permission)
                        <tr>
                            <td>{{ $permission->name }}</td>
                            <td>{{ $permission->description }}</td>
                            <td class="text-center ">
                                <input type="checkbox"
                                       name="permissions[{{ $permission->id }}]"
                                       value="{{ $permission->id }}"
                                        @if(old('permissions['. $permission->id.']') == $permission->id)
                                            checked="checked"
                                        @endif
                                >
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
            </div>
        </form>
    </div>

</div></div></div></div>
@endsection

