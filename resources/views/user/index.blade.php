@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{ route('users.create') }}"
			class="btn btn-primary"
		>New</a>
		<table class="table table-striped">
			<tr>
				<th>
					Nama
				</th>
				<th>
					Email
				</th>
				<th>
					Negeri
				</th>
				<th></th>
			</tr>
			@foreach($pengguna2 as $pengguna)
				<tr>
					<td>
						{{ $pengguna->name }} 
					</td>
					<td>
						{{ $pengguna->email }}
					</td>
					<td>
						{{ $pengguna->state->name ?? '' }}
					</td>
					<td>
						<a class="btn btn-primary" href="{{ route('users.show',[$pengguna->id]) }}">View</a>
						<a class="btn btn-primary" href="{{ route('users.edit',[$pengguna->id]) }}">Edit</a>
						<form action="{{ route('users.destroy',[$pengguna->id]) }}" 
							method="POST" 
							> 
							@csrf
							@method('DELETE')
							<button class="btn btn-warning">Delete</button>
						</form>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
@endsection