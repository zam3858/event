@extends('layouts.app')

@section('content')
	<div class="container">

		<form action="{{ route('users.update',[$pengguna->id]) }}" method="POST">
			@csrf
			@method('PUT')
		  <div class="form-group">
		    <label for="name">Name</label>
		    <input type="text" class="form-control" id="name"
		    	name="name"
		    	value="{{ old('name', $pengguna->name) }}"
		    >
		    @error('name')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
		    @enderror
		  </div>
		  <div class="form-group">
		    <label for="email">Email address</label>
		    <input type="email" class="form-control" id="email" 
		    	name="email"
		    	value="{{ old('email', $pengguna->email) }}"
		    	>
	    	@error('email')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
	    	@enderror
		  </div>

		  <div class="form-group">
		    <label for="name">State</label>
		    <select class="form-control" id="state_id"
		    	name="state_id"
		    >
		    	<option>Sila Pilih</option>
		    	@foreach($states as $state)
		    		<option value="{{ $state->id }}"
		    			@if($state->id == old('state_id', $pengguna->state_id) ) selected @endif
		    			> 
		    			{{ $state->name }} 
		    		</option>
		    	@endforeach
			</select>
		    @error('name')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
		    @enderror
		  </div>

		  <div class="form-group row">
                <table class="table table-hover">
					<thead>
                    <tr class="bg-teal-400">
                        <th>{{ __('Roles') }}</th>
                        <th></th>
                    </tr>
					</thead>
                    <?php
                    $cur_roles = $user->roles->pluck('id')->toArray();
                    ?>
                    @foreach($roles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td class="text-center ">
                                <input type="checkbox"
                                       name="roles[{{ $role->id }}]"
                                       value="{{ $role->id }}"
                                       @if(
                                       (is_array(old('roles')) && in_array($role->id, old('roles')))
                                        || ( in_array($role->id, $cur_roles) )
                                        )
                                            checked="checked"
                                        @endif
                                >
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>

	</div>
@endsection