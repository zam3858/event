<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1>{{ $state->kod }} : {{ $state->name }}</h1>
	<h3>Users:</h3>
	<table style="width:100%;border:1px #000 solid;">
		@foreach($state->users as $user)
		<tr>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>
				<a href="{{ route('users.show', [$user->id]) }}">View</a>
			</td>
		</tr>
		@endforeach
	</table>
</body>
</html>