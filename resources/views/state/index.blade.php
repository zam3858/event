@extends('layouts.app')

@section('content')
	<div class="container">

		<form method="GET" action="">
			<div class="form-group">
		    <label for="name">Name</label>
		    <input type="text" class="form-control" id="search"
		    	name="search"
		    	value="{{ request()->search }}"
		    >
		    <button class="btn btn-primary">Cari</button>
		  </div>
		</form>
		<table class="table table-striped">
			<tr>
				<th></th>
				<th>
					Kod
				</th>
				<th>
					Nama
				</th>
				<th>
					Total Users
				</th>
				<th>
					created At
				</th>
				<th></th>
			</tr>
			@foreach($negeri2 as $negeri)
				@if($loop->first
						&& (
						$negeri2->currentPage() == 1
						)
					)
				<tr>
					<td colspan="6">
						First Record
					</td>
				</tr>
				@endif
				<tr>
					<td>
						{{ $loop->iteration }}
					</td>
					<td> 
						{{ $negeri->kod }} 
					</td>
					<td>
						{{ $negeri->name_display }}
					</td>
					<td>
						{{ $negeri->users->count() }}
					</td>
					<td>
						{{ $negeri->created_at }}
					</td>
					<td>
						{{ $negeri->updated_at->format('d, M Y') }}
					</td>
					<td>
						<a class="btn btn-primary" href="{{ route('states.show',[$negeri->id]) }}">View</a>
						<a class="btn btn-primary" href="{{ route('states.pdf',[$negeri->id]) }}" target="_BLANK">Pdf</a>
						<a class="btn btn-primary" href="{{ route('states.edit',[$negeri->id]) }}">Edit</a>
						<a href="#" onclick="submit_form({{ $negeri->id }})"
							class="btn btn-warning"
							>Delete</a>
						<form action="{{ route('states.destroy',[$negeri->id]) }}" 
							method="POST" 
							id="del_{{ $negeri->id }}"
							> 
							@csrf
							@method('DELETE')
						</form>
					</td>
				</tr>
				@if($loop->last 
					&& (
						$negeri2->lastPage() == $negeri2->currentPage() 
						)
					)
				<tr>
					<td colspan="6">
						Last Record
					</td>
				</tr>
				@endif
			@endforeach
		</table>

		{{ $negeri2->appends(request()->input())->links() }}
	</div>

	<script>
		function submit_form(id) {
			if(confirm('Are you sure?')) {
				$('#del_' + id).submit();
			}
		}
	</script>
@endsection