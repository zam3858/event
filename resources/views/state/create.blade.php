@extends('layouts.app')

@section('content')
	<div class="container">

		<form action="{{ route('states.store') }}" method="POST">
			@csrf

		  <div class="form-group">
		    <label for="email">Kod</label>
		    <input type="text" class="form-control" id="kod" 
		    	name="kod"
		    	value="{{ old('kod') }}"
		    >
		    @error('kod')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
		    @enderror
		  </div>
		  <div class="form-group">
		    <label for="name">Name</label>
		    <input type="text" class="form-control" id="name"
		    	name="name"
		    	value="{{ old('name') }}"
		    >
		    @error('name')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
		    @enderror
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
@endsection
