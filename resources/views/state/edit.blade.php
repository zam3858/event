@extends('layouts.app')

@section('content')
	<div class="container">

		<form action="{{ route('states.update',[$negeri->id]) }}" method="POST">
			@csrf
			@method('PUT')
		  <div class="form-group">
		    <label for="name">Name</label>
		    <input type="text" class="form-control" id="name"
		    	name="name"
		    	value="{{ old('name', $negeri->name) }}"
		    >
		    @error('name')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
		    @enderror
		  </div>
		  <div class="form-group">
		    <label for="kod">Kod</label>
		    <input type="text" class="form-control" id="kod" 
		    	name="kod"
		    	value="{{ old('kod', $negeri->kod) }}"
		    	>
	    	@error('kod')
		    	<span style="color:red;font-size:smaller;">
		    		{{ $message }}
		    	</span>
	    	@enderror
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>

	</div>
@endsection

@section('scripts')
<script src="{{ asset('js/datatable.js') }}" defer></script>

	<script type="text/javascript">
		alert('hello');
	</script>
@endsection

@section('styles')
<style type="text/css">
	h1 {
		color:red;
	}
</style>
@endsection