<?php

use Illuminate\Support\Facades\Route;
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// http://event.test/home
// http://event.test/login
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



// http://localhost:8000/users/index

	// Route::get('/users', 'UserController@index')
	//  					->name('users.index');


	// Route::get('/users', 'UserController@index')
	// 					->name('users.index');

	// Route::get('/users/create', 'UserController@create')
	// 					->name('users.create');

	// Route::get('/users/{user}', 'UserController@show')
	// 					->name('users.show');

	// Route::post('/users', 'UserController@store')
	// 					->name('users.store');

	// Route::get('/users/{user}/edit', 'UserController@edit')
	// 					->name('users.edit');

	// Route::put('/users/{user}', 'UserController@update')
	// 					->name('users.update');

	// Route::delete('/users/{user}', 'UserController@destroy')
// 					->name('users.destroy');
// http://localhost/welcome

//http://event.test/admin/states
Route::middleware(['auth'])
	// ->prefix('admin')
	// ->namespace('Admin')
	->group(function() {



	Route::resource('users','UserController');

	Route::resource('roles', 'RoleController');
	//Route::resource('users','StateController');
	//Route::get('welcome/{id}/{date?}', 'UserController@sayhi');

});

Route::get('/maklumat_pengguna/{user}', 'UserController@getdetail');

Route::get('/demo', 'StateController@demo');

Route::get('states/{state}/users', 'StateController@get_users')->name('states.users');

Route::get('states/{state}/pdf', 'StateController@export_pdf')->name('states.pdf');

Route::get('states/{state}/excel', 'StateController@export_excel')->name('states.excel');

Route::get('states/{state}/worksheet', 'StateController@export_multiplesheet')->name('states.export_multiplesheet');
Route::resource('states','StateController');

Route::get('/get_user', function() {
	return \DB::select("SELECT users.name as user_name, states.name as state_name, states.*, users.* 
						FROM users, states 
						WHERE  1=1
						AND users.state_id = states.id
						");

});

Route::get('/get_user2', function() {
	return \App\Model\State::with('users')->get();

});

Route::get('/test1', function() {
	//return App\Model\State::first()->kod;
	return App\Model\State::first();
});

Route::get('/test2', function() {

	$state = new App\Model\State();
	$state->name = "Pahang22";
	$state->kod = "Phg22";

	$state->save();

	$user = new App\User();
	$user->name = "abcde";
	$user->email = "fff@fefe.com";
	$user->password = \Hash::make('password');
	$user->state_id = $state->id;
	$user->save();
	//$state->users()->save($user);


	return $user;



});

Route::get('excelview', 'UserController@export_view')->name('users.excel.view');

Route::get('perm', function() {

 $admin = Role::where('name','admin')->first();
 $permission = Permission::where('name', 'manage user')->first();
 
 $admin->givePermissionTo($permission);

 $user = \App\User::first();
 $user->roles()->save($admin);

});