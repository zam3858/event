<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class KadPengenalan implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $length = strlen($value);
        $isnumber = is_numeric($value);
        return ( ($length == 12) && $isnumber );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Masukkan kad pengenalan 12 digit tanpa -.';
    }
}
