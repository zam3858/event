<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    
    //protected $connection = '';
	protected $table = 'states';
	protected $primaryKey = 'id';
	//protected $hidden = ['kod','name'];

	//protected $dates = ['DOB'];

	public function getCreatedAtAttribute($value)
    {
    	return $value;
    	// \Carbon\Carbon::setLocale('ms_MY');
    	// $created = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value);
    	// return $created->isoFormat('Do MMMM YYYY') ;
        //return ucfirst($value);
    }

    public function getNameDisplayAttribute()
    {
    	return strtoupper($this->name) ;
        //return ucfirst($value);
    }

    public function setNameAttribute($value)
    {
    	$this->attributes['name'] = strtoupper($value) ;
    }

    public function users() {
    	return $this->hasMany('App\User','state_id','id');
    }
}
