<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenRep extends Command
{
    /**
     * The name and signature of the console command.
     * email:send {user}
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * $userId = $this->argument('user');
     * $name = $this->ask('What is your name?');
     * $password = $this->secret('What is the password?');
     * $this->info('Display this on the screen');
     * $headers = ['Name', 'Email'];
     * $users = App\User::all(['name', 'email'])->toArray();
     * $this->table($headers, $users);
     * @return int
     */
    public function handle()
    {
        //
    }
}
