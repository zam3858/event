<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ProgUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:progress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'email semua user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Welcome to user email utility");
        $this->error("******************************");
        $this->line("");

        $users = \App\User::get();
       
        $bar = $this->output->createProgressBar($users->count());

        $bar->start();

        foreach($users as $user) {
        	$email_info['user'] = $user;
	        $email_info['password'] = "abc123";
	        \Mail::to($user)->send( 
	                new \App\Mail\UpdateUser($email_info) 
	            );

        	$bar->advance();
        }

        $bar->finish();
        
    }
}
