<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate report senarai negeri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $state = \App\Model\State::with('users')->first();

        $pdf = \PDF::loadView('state.pdf', [ 'state' => $state ])
                                ->setPaper('a4', 'portrait')
        ;
        $pdf->save(storage_path('app/public/stored.pdf'));
    }
}
