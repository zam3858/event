<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ListUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Senaraikan pengguna';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Welcome to user search utility");
        $this->error("******************************");
        $this->line("");

        do {

            $name = $this->ask("Cari siapa?");

            $headers = ['Name', 'Email'];
            $users = \App\User::select(['name','email'])
                            ->where('name', 'LIKE', "%". $name ."%")
                            ->get()
                            ->toArray();
            $this->table($headers, $users);

        } while( $this->confirm('Teruskan?') );
    }
}
