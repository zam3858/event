<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\State;
use App\Http\Requests\State\Update;
use App\Http\Requests\State\Store;
use App\Exports\MultipleSheet;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->search;
        //dapatkan data dari table state
        //$id = [1,2,4,6];
        $negeri2 = State::when($search, function($query) use ($search) {

                            $query->where(function($query) use ($search){
                                $query->where('name','LIKE', '%'. $search . '%')
                                      ->orWhere('kod','LIKE', '%'. $search . '%');
                            });
                        })
                        // ->orWhereHas('users', function($query) use ($search){
                        //         $query->where('name','LIKE', '%'. $search . '%')
                        //               ->orWhere('email','LIKE', '%'. $search . '%');
                        //     })
                        // ->with(['users'])
                        // ->whereYear('created_at', '2020')
                        ->orderBy('name','ASC')
                        //->whereIn('id',$id)
                        ->paginate(2);
        
        //gunakan view untuk papar data
        return view('state.index', [ 'negeri2' => $negeri2 ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('state.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        
        //prepare row baru
        $state = new State();

        //dapatkan dan isi data
        $state->name = request()->name;
        $state->kod = request()->kod;

        //simpan data dalam db
        $state->save();

        session()->flash('notification', "Rekod updated");

        //redirect ke /states/index
        return redirect( route('states.index') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        return view('state.show', [ 'negeri' => $state ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        return view('state.edit', [ 'negeri' => $state ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, State $state)
    {
        
        //assign value baru
        $state->name = request()->name;
        $state->kod = request()->kod;
        //save
        $state->save();

        session()->flash('notification', "Rekod updated");

        //redirect ke index
        return redirect( route('states.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        $state = State::find($id);
        $state->delete();

        session()->flash('notification', "Rekod deleted");

        return redirect( route('states.index') );
    }

    public function export_pdf(State $state) {

        $state->load('users');

        $pdf = \PDF::loadView('state.pdf', [ 'state' => $state ])
                                ->setPaper('a3', 'landscape')
        ;
        return $pdf->download( $state->name . '_' . date('Y-m-d') . '.pdf');
    }

    public function export_excel() {

        return \Excel::download(new \App\Exports\StateExport(), 
            'mystates.xlsx');
    }

    public function export_multiplesheet() {

        return \Excel::download(new MultipleSheet(), 
            'multiple.xlsx');
    }

    public function demo() {
      $states = State::get();
      return view('demo', compact('states'));  
    }

    public function get_users(State $state) {
        $state->load('users');
        return view('demo2', compact('state')) ;
    }

}
