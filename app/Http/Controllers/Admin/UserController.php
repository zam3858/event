<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\User\Update;
use App\Http\Requests\User\Store;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dapatkan data dari table user
        $pengguna2 = User::get();
        //gunakan view untuk papar data
        return view('user.index', [ 'pengguna2' => $pengguna2 ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        
        //prepare row baru
        $user = new User();

        //dapatkan dan isi data
        $user->name = request()->name;
        $user->email = request()->email;
        $user->password = \Hash::make( $request->password );

        //simpan data dalam db
        $user->save();

        session()->flash('notification', "Rekod updated");

        //redirect ke /users/index
        return redirect( route('users.index') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$pengguna = User::where('id', $id)->first();
        $pengguna = User::find($id);
        return view('user.show', [ 'pengguna' => $pengguna ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengguna = User::find($id);
        return view('user.edit', [ 'pengguna' => $pengguna ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {

        //dapatkan rekod yang nak diupdate
        $user = User::find($id);
        //assign value baru
        $user->name = request()->name;
        $user->email = request()->email;
        //save
        $user->save();

        session()->flash('notification', "Rekod updated");

        //redirect ke index
        return redirect( route('users.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        session()->flash('notification', "Rekod deleted");

        return redirect( route('users.index') );
    }

    function greet($name, $date = 'today') {
        dd(1);
        dd("hello," . $name . ' date is ' . $date);
    }

    function sayhi($name, $date = 'today') {

        dd("hello there! sayhi");
    }
}
