<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use \App\Model\State;
use App\Http\Requests\User\Update;
use App\Http\Requests\User\Store;
use App\Exports\UserViewExport;
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (!auth()->user()->hasPermissionTo('manage user')) {
            abort(403, "Access denied");
        }

        //dapatkan data dari table user
        $pengguna2 = User::select(\DB::raw('users.*'))
                        ->with('state')
                        ->join('states', 'users.state_id', '=', 'states.id')
                        ->orderBy(\DB::raw('states.name'))
                        ->get();


        //gunakan view untuk papar data
        return view('user.index', [ 'pengguna2' => $pengguna2 ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('create user')) {
            abort(403, "Access denied");
        }

        $roles = Role::orderBy('name')->get();
        
        $states = State::orderBy('name')->get();
        return view('user.create', ['states' => $states,
            'roles' => $roles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        
        //prepare row baru
        $user = new User();
        //$user->id = \Str::uuid();
        //dapatkan dan isi data
        $user->name = request()->name;
        $user->email = request()->email;
        $user->password = \Hash::make( $request->password );

        //simpan data dalam db
        $user->save();

        if(!empty($request->roles)) {
            $roles = Role::whereIn('id', $request->roles)->get();
            $user->syncRoles($roles);
        }

        session()->flash('notification', "Rekod updated");

        //redirect ke /users/index
        return redirect( route('users.index') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show', [ 'pengguna' => $user ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $states = State::orderBy('name')->get();
        $roles = Role::orderBy('name')->get();
        return view('user.edit', [ 'pengguna' => $user, 'states' => $states ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, User $user)
    {
        //assign value baru
        $user->name = request()->name;
        $user->email = request()->email;
        $user->state_id = request()->state_id;

        //save
        $user->save();

        if(!empty($request->roles)) {
            $roles = Role::whereIn('id', $request->roles)->get();
            $user->syncRoles($roles);
        }

        session()->flash('notification', "Rekod updated");
        $email_info['user'] = $user;
        $email_info['password'] = "abc123";
        \Mail::to($user)->send( 
                new \App\Mail\UpdateUser($email_info) 
            );

        //redirect ke index
        return redirect( route('users.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->delete();

        session()->flash('notification', "Rekod deleted");

        return redirect( route('users.index') );
    }


    function getdetail(User $user) {
        return $user;
    }

    public function export_view() {

        return \Excel::download(new UserViewExport(), 
            'madeWithView.xlsx');
    }

}
