<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\KadPengenalan;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //http://event.test/users/1
        $id = request()->segment(2);
        return [
            'name' => [
                        'required',
                        new KadPengenalan()
                        ],
            'email' => ['required',
                        'email',
                        //Rule::unique('users')->ignore($id),
                        function ($attribute, $value, $fail) use ($id) {
                            $user = \App\User::where('email', $value)
                                        ->where('id','!=', $id)
                                        ->first();
                            if (!empty($user)) {
                                $fail($attribute.' is already in use.');
                            }
                        }
                    ],
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Maklumat ini diperlukan',
            'email' => 'format email adalah xxx@xxx.com'
        ];
    }
}
