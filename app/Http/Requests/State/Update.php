<?php

namespace App\Http\Requests\State;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->segment(2);
        return [
            'name' => [
                        'required'
                        ],
            'kod' => ['required',
                        Rule::unique('states')->ignore($id),
                    ],
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Maklumat ini diperlukan',
        ];
    }
}
