<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleSheet implements WithMultipleSheets
{

	/**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
        
        $users = \App\User::limit(5)->get();
        $sheets[] = new StateExport();
        $sheets[] = new UserExport($users);

        return $sheets;
    }
}
