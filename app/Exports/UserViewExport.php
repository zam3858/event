<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\User;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

class UserViewExport implements FromView, WithEvents
{
    public function view(): View
    {
        return view('excel.user', [
            'user' => User::first()
        ]);
    }

    public function registerEvents(): array 
    {
        
        return [
            AfterSheet::class => function (AfterSheet $event) {

            //column's width
            $event->sheet->getDelegate()
                        ->getColumnDimension('A')
                        ->setWidth(15);

            //mergecell
            $event->sheet->getDelegate()                     
                        ->mergeCells('C1:C2')
                        ->mergeCells('D1:D2');
                        // ->mergeCells('B1:B2');

            $event->sheet->getDelegate()
                                ->getStyle('A1:B3')//pCellCoordinates
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setARGB('ffff00');

            $event->sheet->getDelegate()
                                ->getStyle('A1:B3')
                                ->getBorders()->getOutline()
                                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

             $event->sheet->getDelegate()
                                ->getStyle('A1:A3') //pCellCoordinates
                                ->getFont()
                                ->setBold(true)
                                ->setSize(14);

            },
        ];
    }
}
