<?php

namespace App\Exports;

use App\Model\State;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromCollection, WithHeadings
{

	public function __construct($user) {
		$this->user = $user;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->user;
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'email',
            '--',
            'created_at',
            'updated_at',
            'state_id'
        ];
    }
}
