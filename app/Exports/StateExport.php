<?php

namespace App\Exports;

use App\Model\State;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class StateExport implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$states = collect(
    					\DB::select(\DB::raw("SELECT * FROM states")) 
    				);
        return $states;
    }

    // utk bg nama kat tab dlm page excel sheet tu
    public function title():string  
    {
        return 'Senarai State';
    }
}
