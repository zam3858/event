<?php


namespace App\Exports\Negeri_Tengah_BPM;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
//use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;

class KL_Index implements WithEvents,
FromView,
// ShouldAutoSize,
WithTitle
{
    public function __construct()
    {
    }

    public function view(): View{

       // $subitemgroups = $this->subitemgroups;
       // $expenditures = $this->expenditures;
       // $items = $this->items;
        return view('excel.someview');
        
    }

    public function title():string  // utk bg nama kat tab dlm page excel sheet tu
    {
        return 'Title';
    }

    public function registerEvents(): array{

        
        return[
            AfterSheet::class => function (AfterSheet $event)
                 {

            //column's width
            $event->sheet->getDelegate()
                        ->getColumnDimension('A')
                        ->setWidth(5);

            //mergecell
            $event->sheet->getDelegate()                     
                        ->mergeCells('C1:C2')
                        ->mergeCells('D1:D2');
                        // ->mergeCells('B1:B2');

            //Jenis tulisan
                $event->sheet->getDelegate()
                                ->getStyle('A1:SR3') //pCellCoordinates
                                ->getFont()
                                ->setBold(true)
                                ->setSize(14); //pValue
                                
            //Color column-kuning
                $event->sheet->getDelegate()
                                ->getStyle('E4:AZ1230')//pCellCoordinates
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID) //pValue
                                ->getStartColor()
                                ->setARGB('ffff00'); //pValue

            //Border
                $event->sheet->getDelegate()
                                // ->getStyle('E4:E1027' . (1 + $maingroups_count ))
                                ->getStyle('E4:E1027')
                                ->getBorders()->getOutline()
                                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $event->sheet->getDelegate()
                ->getStyle('A3:SQ3')
                ->getBorders()->getBottom()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


                     //Color chain link
              $event->sheet->getDelegate()
              ->getStyle('F1:F1027')//pCellCoordinates
              ->getFill()
              ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID) //pValue
              ->getStartColor()
              ->setARGB('ffff00'); //pValue
            },
        ];
    }
}
