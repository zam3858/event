<?php

use Illuminate\Database\Seeder;

use App\Model\State;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('states')->truncate();
        State::create([
        	'kod' => "TRG",
        	'name' => "Terengganu",
        ]);
        
        State::create([
        	'kod' => "PHG",
        	'name' => "Pahang",
        ]);

	    State::create([
        	'kod' => "JHR",
        	'name' => "Johor",
        ]);

        State::create([
        	'kod' => "MLK",
        	'name' => "Melaka",
        ]);



    }
}
